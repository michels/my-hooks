#!/bin/bash

branch=$(git rev-parse --abbrev-ref HEAD)
version=$(poetry version -s)

if [[ $branch == "main" ]]; then
    poetry run python -m twine upload dist/*-"${version}"*
fi
