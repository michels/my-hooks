#!/bin/bash

changed_files=$(git diff --cached --name-only --diff-filter=ACM | grep -E '\.py$')

if [ -n "$changed_files" ]; then
	poetry run sphinx-apidoc -e -o docs/source $changed_files
	make -C docs html
fi
