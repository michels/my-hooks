#!/bin/bash

branch=$(git rev-parse --abbrev-ref HEAD)

if [[ $branch == "main" ]]; then
    git tag "$(poetry version -s)"
else
    echo "Skipping creation of git tag, as not on main branch."
fi
