#!/bin/bash

changed_files=$(git diff --cached --name-only --diff-filter=ACM | grep -E '\.py$')

if [ -n "$changed_files" ]; then
	poetry run black -l 79 $changed_files
	git add $changed_files
fi
