#!/bin/bash

branch=$(git rev-parse --abbrev-ref HEAD)

if [[ $branch == "main" ]]; then
    poetry version patch
    git add pyproject.toml
else
    echo "Skipping poetry version dump, as not on main branch."
fi
